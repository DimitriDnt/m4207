int compteur;

void setup() {
  // put your setup code here, to run once:
  int compteur = 0;
  Serial.begin(9600);
  Serial.println("C'est Parti !");
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  Serial.print("La valeur du compteur est : ");
  Serial.println(compteur);
  delay(250);
  compteur = compteur + 1;
}
