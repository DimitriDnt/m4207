void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13,HIGH);
  Serial.println("La LED S'allume");
  delay(100);
  
  digitalWrite(13,LOW);
  Serial.println("La LED s'éteint");
  delay(100);
}
