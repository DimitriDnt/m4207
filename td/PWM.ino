int capteur = A0;
int led = 3;
int valeurCapteur = 0;
int valeurLed = 0;

void setup() {
  Serial.begin(9600);
  pinMode(capteur,INPUT);
  pinMode(led,OUTPUT);

}

void loop() {

  valeurCapteur = analogRead(capteur);
  valeurLed = map(valeurCapteur,0,1023,0,255);
  analogWrite(led, valeurLed);
  Serial.print("Valeur du capteur = ");
  Serial.println(valeurCapteur);
  Serial.print("Valeur de la LED = ");
  Serial.println(valeurLed);
  
}
