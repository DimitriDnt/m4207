int led = 6;
int button = 2;
int buttonState = 0;

void setup() {
  pinMode(led,OUTPUT);
  pinMode(button,INPUT);
}

void loop() {

  buttonState = digitalRead(button);

  if (buttonState == HIGH) {
  digitalWrite(led,HIGH);
  }

  if (buttonState == LOW) {
  digitalWrite(led,LOW);
  }
  
}
